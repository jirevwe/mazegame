﻿using UnityEngine;

public class GuardBehaviour : MonoBehaviour {

	Animator anim;
	public GameObject explosionPrefab;
	private FieldOfView fov;
	private AIPath aiPath;
	
	[HideInInspector] public GameObject currentTarget;
	[HideInInspector] public GameObject player;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindWithTag("Player");
		fov = gameObject.GetComponent<FieldOfView> ();
		aiPath = gameObject.GetComponent<AIPath> ();
		anim = GetComponent<Animator>();
	}
}
