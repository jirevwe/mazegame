﻿using System;
using UnityEngine;

public class AICustomDetection : MonoBehaviour {

    [SerializeField] 
    private GameObject[] collisonTiles;
    [SerializeField]
    private Material inactiveMaterial;
    [SerializeField]
    private Material activeMaterial;

    private int[][] pattern4 = {
                                    new int[]{-1,0},
                                    new int[]{1,0}
                               };

    private int[][] pattern3 = {
                                    new int[]{-1,-1},
                                    new int[]{1,-1},
                                    new int[]{-1,1},
                                    new int[]{1,1}
                               };

    private int[][] pattern2 = {
                                    new int[]{-1,0},
                                    new int[]{0,1},
                                    new int[]{1,0},
                                    new int[]{0,-1}
                               };

    private int[][] pattern1 = {    
                                    new int[]{-1,-1},
                                    new int[]{-1,0},
                                    new int[]{-1,1},
                                    new int[]{0,-1},
                                    new int[]{0,1},
                                    new int[]{1,-1},
                                    new int[]{1,0},
                                    new int[]{1,1},
                               };

    void Leave(string[] details)
    {
        Leave(details[0], details[1]);
    }

    void Leave(string colNum, string rowNum)
    {
        foreach (int[] patternRow in pattern2)
        {
            int newRowNum = int.Parse(rowNum) + patternRow[1];
            int newColNum = int.Parse(colNum) + patternRow[0];
            try
            {
                collisonTiles[newRowNum - 1].transform.FindChild("col" + newColNum).gameObject.GetComponent<Renderer>().material = inactiveMaterial;
            }
            catch (NullReferenceException) { }
            catch (IndexOutOfRangeException) { }
        }
    }

    void Pattern(string[] details)
    {
        Pattern(details[0], details[1]);
    }

    void Pattern(string colNum, string rowNum)
    {
        foreach (int[] patternRow in pattern2)
        {
            int newRowNum = int.Parse(rowNum) + patternRow[1];
            int newColNum = int.Parse(colNum) + patternRow[0];
            try
            {
                collisonTiles[newRowNum - 1].transform.FindChild("col" + newColNum).gameObject.GetComponent<Renderer>().material = activeMaterial;
            }
            catch (NullReferenceException) { }
            catch (IndexOutOfRangeException) { }
        }
    }
}
