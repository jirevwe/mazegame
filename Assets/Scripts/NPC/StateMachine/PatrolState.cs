﻿using UnityEngine;
using System.Collections;

public class PatrolState : IEnemyState {

	private readonly NPCStateMachineManager enemy;

	public PatrolState (NPCStateMachineManager statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState(){
		Patrol ();
		Look ();
	}

	public void ToPatrolState(){

	}
	
	public void ToAlertState(){
		enemy.currentState = enemy.alertState;
	}
	
	public void ToChaseState(){
        enemy.chaseState.currentShootCounter = 0f;
		enemy.currentState = enemy.chaseState;
	}

	public void ToExplodeState(){
		enemy.currentState = enemy.explodeState;
	}

	void Look(){
		if (enemy.fov.visibleTargets.Count > 0) {
			ToChaseState();
		}
	}

	void Patrol(){
		enemy.aiPath.speed = enemy.patrolSpeed;
		enemy.fov.viewAngle = enemy.patrolViewAngle;
		enemy.fov.viewRadius = enemy.patrolViewDistance;
		enemy.anim.SetFloat("Speed", enemy.patrolSpeed);
		enemy.aiPath.target = enemy.currentTarget.transform;
	}
}
