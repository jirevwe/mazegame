﻿using UnityEngine;

public class ChaseState : IEnemyState {

	private readonly NPCStateMachineManager enemy;
	private float endTime = 0f;
    public float currentShootCounter = 0f;

    public ChaseState (NPCStateMachineManager statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState(){
		Chase ();
		CountDown ();
		Look ();
	}

	public void ToPatrolState(){

	}
	
	public void ToAlertState(){
		enemy.currentState = enemy.alertState;
		endTime = 0f;
	}
	
	public void ToChaseState(){

	}
	
	public void ToExplodeState(){
		enemy.currentState = enemy.explodeState;
	}

	void Chase(){
		enemy.fov.viewAngle = enemy.chaseViewAngle;
		enemy.aiPath.speed = enemy.chaseSpeed;
	    enemy.fov.viewRadius = enemy.chaseViewDistance;
		enemy.aiPath.target = enemy.player.transform;
		enemy.anim.SetFloat("Speed", enemy.chaseSpeed);

        if(enemy.isThrower && currentShootCounter >= enemy.shootRate)
        {
            var fire = Object.Instantiate(enemy.fireThorwerPrefab, enemy.transform.position, Quaternion.identity) as GameObject;
            fire.GetComponent<FireBall>().player = enemy.player;
            currentShootCounter = 0f;
        }

        currentShootCounter += Time.deltaTime;
	}

	void CountDown(){
		if (endTime == 0)
			endTime = (int)(Time.time + enemy.timeBeforeExplode);
		if ((int)(Time.time) == (int)(endTime) && Vector3.Distance(enemy.transform.position, enemy.player.transform.position) <= enemy.chaseViewDistance / 2)
        {
			ToExplodeState();
		}
        else if((int)(Time.time) == (int)(endTime) && Vector3.Distance(enemy.transform.position, enemy.player.transform.position) > enemy.chaseViewDistance / 2)
        {
            endTime = 0f;
        }
	}

	void Look(){
		if (enemy.fov.visibleTargets.Count > 0) {
			ToChaseState ();
		} else {
			ToAlertState ();
		}
	}
}