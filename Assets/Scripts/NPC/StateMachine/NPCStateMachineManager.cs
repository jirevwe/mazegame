﻿using UnityEngine;

public class NPCStateMachineManager : MonoBehaviour {

	public float timeBeforeExplode = 3f;
    public float shootRate = 5f;

	public float searchingDuration = 3f;
	
	public float chaseViewDistance = 10f;
	public float patrolViewDistance = 7f;
	
	public float chaseViewAngle = 360f;
	public float patrolViewAngle = 45f;

	public float chaseSpeed = 6f;
	public float patrolSpeed = 3f;

    public bool isThrower;
	
	public GameObject explosionPrefab;
	public GameObject currentTargetPrefab;
    public GameObject fireThorwerPrefab;

    [HideInInspector] public IEnemyState currentState;
	[HideInInspector] public ChaseState chaseState;
	[HideInInspector] public AlertState alertState;
	[HideInInspector] public PatrolState patrolState;
	[HideInInspector] public ExplodeState explodeState;

    [HideInInspector] public GameObject player;
	[HideInInspector] public FieldOfView fov;
	[HideInInspector] public AIPath aiPath;
	[HideInInspector] public Animator anim;
	[HideInInspector] public GameObject currentTarget;

	void Reset()
	{
		timeBeforeExplode = 3f;
	
		searchingDuration = 3f;
	
		chaseViewDistance = 10f;
		patrolViewDistance = 7f;
	
		chaseViewAngle = 360f;
		patrolViewAngle = 45f;

		chaseSpeed = 6f;
		patrolSpeed = 3f;

	}

	void Awake(){
		alertState = new AlertState (this);
		chaseState = new ChaseState (this);
		patrolState = new PatrolState (this);
		explodeState = new ExplodeState (this);

		player = GameObject.FindWithTag("Player");
		fov = gameObject.GetComponent<FieldOfView> ();
		aiPath = gameObject.GetComponent<AIPath> ();
		anim = GetComponent<Animator>();

		currentTarget = currentTargetPrefab;
	}

	// Use this for initialization
	void Start () {
		currentState = patrolState;
	}
	
	// Update is called once per frame
	void Update () {
		currentState.UpdateState ();
	}
}
