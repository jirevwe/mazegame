﻿using UnityEngine;
using System.Collections;

public class ExplodeState : IEnemyState {

	private readonly NPCStateMachineManager enemy;

	public ExplodeState (NPCStateMachineManager statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState(){
		Explode ();
	}

	public void ToPatrolState(){
		
	}
	
	public void ToAlertState(){

	}
	
	public void ToChaseState(){

	}
	
	public void ToExplodeState(){
		
	}

	void Explode(){
		GameObject explosion = Object.Instantiate(enemy.explosionPrefab, enemy.transform.position, Quaternion.identity) as GameObject;
		enemy.gameObject.SetActive(false);
	}

}