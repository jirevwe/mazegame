﻿using UnityEngine;

public class AlertState : IEnemyState {

	private readonly NPCStateMachineManager enemy;
	private float seacrhTime = 0f;

	public AlertState (NPCStateMachineManager statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState(){
		Search ();
		Look ();
	}

	public void ToPatrolState(){
		enemy.currentState = enemy.patrolState;
		seacrhTime = 0f;
	}
	
	public void ToAlertState(){

	}
	
	public void ToChaseState(){
        enemy.chaseState.currentShootCounter = 0f;
        enemy.currentState = enemy.chaseState;
		seacrhTime = 0f;
    }
	
	public void ToExplodeState(){
		enemy.currentState = enemy.explodeState;
	}

	void Look(){
		if (enemy.fov.visibleTargets.Count > 0) {
			ToChaseState();
		}
	}

	void Search(){
		seacrhTime += Time.deltaTime;

		if (seacrhTime >= enemy.searchingDuration) {
			ToPatrolState();
		}
	}
}
