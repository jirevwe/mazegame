﻿using UnityEngine;

public class FireBall : MonoBehaviour
{
    [SerializeField]
    private float fireBallSpeed = 0.5f;
    [HideInInspector]
    public GameObject player;
    public GameObject ragdollPrefab;

    // Use this for initialization
    void Start()
    {
        transform.LookAt(player.transform);
        Destroy(gameObject, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * fireBallSpeed);
    }

    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player" || collider.tag == "Walls")
        {
            if (collider.tag == "Player")
            {
                collider.gameObject.SetActive(false);

                GameObject deadPlayer = Instantiate(ragdollPrefab, collider.transform.position, Quaternion.identity) as GameObject;
                deadPlayer.gameObject.GetComponentInChildren<Rigidbody>().AddRelativeForce(Vector3.Cross(transform.position, collider.transform.position), ForceMode.Impulse);
                
                GameController.GameControllerInstance.playerSightedMode = GameController.PlayerSightedModes.NotSeen;
            }

            Destroy(gameObject);
        }
    }
}
