﻿using UnityEngine;

public class EnemyWaypointPlacer : MonoBehaviour {

	GameObject patrolPointHolder;

	private Transform[] allPatrolPoints;

    [SerializeField]
    float counter = 0f;
	[SerializeField]
    float timeToCount = 10f;

	bool done = false;

    [SerializeField]
    Vector3[] waypoints = new Vector3[2];
    [SerializeField]
    int current = 0;

	void Awake(){
		patrolPointHolder = GameObject.FindGameObjectWithTag ("TileHolder");
	}

	void Repath()
	{
        transform.position = waypoints[current];
	}

	void Start()
	{
		Invoke("AssignWaypoint", 0.5f);
	}

	void AssignWaypoint () {
		allPatrolPoints = patrolPointHolder.transform.GetComponentsInChildren<Transform>();

        for (int i = 0; i < waypoints.Length; i++)
        {
            waypoints[i] = allPatrolPoints[Maze.MazeInstance.randInstance.Next(0, allPatrolPoints.Length)].position;
        }

        done = true;
	}
	
	void Update () {
		if(counter >= timeToCount && done == true)
		{
            if (current < waypoints.Length - 1)
                current++;
            else if(current == waypoints.Length - 1)
                current = 0;

            Repath();
			counter = 0f;
		}
		counter += Time.deltaTime;
	}
}
