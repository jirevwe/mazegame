﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour {

	[Range (0, 100)]
	public float viewRadius;
	[Range(0, 360)]
	public float viewAngle;

	public LayerMask targetMask;
	public LayerMask obstacleMask;

	public int edgeResolveIterations;
	public float meshResolution;
	public float edgeDistanceThreshold;

	public MeshFilter meshFilter;

	public MeshRenderer meshRenderer;

	public Material[] fovMaterial;
	private Mesh mesh;

	[HideInInspector]
	public List<Transform> visibleTargets = new List<Transform>();

	void Start(){
		mesh = new Mesh ();
		mesh.name = "VIEW MESH";
		meshFilter.mesh = mesh;
		meshRenderer.material = fovMaterial [0];
		StartCoroutine ("FindTargetsWithDelay", .2f);
	}

	IEnumerator FindTargetsWithDelay(float delay){
		while (true) {
			yield return new WaitForSeconds(delay);
			FindVisibleTargets();
		}
	}

	void LateUpdate(){
		DrawFieldOfView ();
	}

	void Update(){
		
	}

	ViewCastInfo ViewCast(float globalAngle){
		Vector3 dir = DirFromAngle (globalAngle, true);
		RaycastHit hit;

		if (Physics.Raycast (transform.position, dir, out hit, viewRadius, obstacleMask)) {
			return new ViewCastInfo (true, hit.point, hit.distance, globalAngle);
		} else {
			return new ViewCastInfo (false, transform.position + dir * viewRadius, viewRadius, globalAngle);
		}
	}

	EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast){
		float minAngle = minViewCast.angle;
		float maxAngle = maxViewCast.angle;
		Vector3 minPoint = Vector3.zero;
		Vector3 maxPoint = Vector3.zero;

		for (int i = 0; i < edgeResolveIterations; i++) {
			float angle = (minAngle + maxAngle) / 2;
			ViewCastInfo newViewCast = ViewCast(angle);

			bool edgeDistanceThresholdExceeded = Mathf.Abs(minViewCast.dst - maxViewCast.dst) > edgeDistanceThreshold;
			if(newViewCast.hit == minViewCast.hit && !edgeDistanceThresholdExceeded){
				minAngle = angle;
				minPoint = newViewCast.point; 
			}else{
				maxAngle = angle;
				maxPoint = newViewCast.point;
			}
		}
		return new EdgeInfo (minPoint, maxPoint);
	}

	void DrawFieldOfView(){
		int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
		float stepAngleSize = viewAngle / stepCount;
		List<Vector3> viewPoints = new List<Vector3> ();
		ViewCastInfo oldViewCast = new ViewCastInfo();

		for (int i = 0; i <= stepCount; i++) {
			float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
			ViewCastInfo newViewCast = ViewCast(angle);

			if(i > 0){
				bool edgeDistanceThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDistanceThreshold;
				if(oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDistanceThresholdExceeded)){
					EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
					if(edge.pointA != Vector3.zero){
						viewPoints.Add(edge.pointA);
					}
					if(edge.pointB != Vector3.zero){
						viewPoints.Add(edge.pointB);
					}
				}
			}

			viewPoints.Add(newViewCast.point);
			oldViewCast = newViewCast;
		}

		int vertexCount = viewPoints.Count + 1;
		Vector3[] vertices = new Vector3[vertexCount];
		int[] triangles = new int[(vertexCount - 2) * 3];

		vertices [0] = Vector3.zero;
		for (int i = 0; i < vertexCount - 1; i++) {
			vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

			if(i < vertexCount - 2){
				triangles[i * 3] = 0;
				triangles[i * 3 + 1] = i + 1;
				triangles[i * 3 + 2] = i + 2;
			}
		}

		mesh.Clear ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.RecalculateNormals ();
	}

	void FindVisibleTargets(){
		visibleTargets.Clear ();
		Collider[] targetsInViewRadius = Physics.OverlapSphere (transform.position, viewRadius, targetMask); 

		for (int i = 0; i < targetsInViewRadius.Length; i++) {
			Transform target = targetsInViewRadius[i].transform;
			Vector3 dirTotarget = (target.position - transform.position).normalized;
			if(Vector3.Angle(transform.forward, dirTotarget) < viewAngle / 2){
				float dstToTarget = Vector3.Distance(transform.position, target.position);
				if(!Physics.Raycast(transform.position, dirTotarget, dstToTarget, obstacleMask)){
					visibleTargets.Add(target);
				}
			}
		}
		if (visibleTargets.Count > 0) {
			meshRenderer.material = fovMaterial[1];
			transform.LookAt (GetComponent<NPCStateMachineManager>().player.transform);
		}else{
			meshRenderer.material = fovMaterial[0];
		}
	}

	public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
	{
		if (!angleIsGlobal)
			angleInDegrees += transform.eulerAngles.y;
		return new Vector3 (Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
	}

	public struct ViewCastInfo{
		public bool hit;
		public Vector3 point;
		public float dst;
		public float angle;

		public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle){
			hit = _hit;
			point = _point;
			dst = _dst;
			angle = _angle;
		}
	}

	public struct EdgeInfo{
		public Vector3 pointA;
		public Vector3 pointB;

		public EdgeInfo(Vector3 pA, Vector3 pB){
			pointA = pA;
			pointB = pB;
		}		
	}

}
