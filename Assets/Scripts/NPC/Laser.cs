﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour
{

    public bool isOff;
    private int[] viewTimes;
    private BoxCollider laserBoxCollider;
    private MeshRenderer laserMeshRenderer;

    void Awake()
    {
        laserMeshRenderer = gameObject.GetComponent<MeshRenderer>();
        laserBoxCollider = gameObject.GetComponent<BoxCollider>();
    }

	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(Blink(2));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(PlayerBehaviour.instance.Die());
            GameController.GameControllerInstance.DisplayPlayerDeadText();
        }
    }

    IEnumerator Blink(int timeToWait)
    {
        yield return new WaitForSeconds(Random.Range(2, timeToWait + 1));
        if (isOff)
        {
            laserBoxCollider.enabled = true;
            laserMeshRenderer.enabled = true;
            isOff = !isOff;
            StartCoroutine(Blink(3));
        }
        else
        {
            isOff = !isOff;
            laserBoxCollider.enabled = false;
            laserMeshRenderer.enabled = false;
            StartCoroutine(Blink(5));
        }
    }
}
