﻿using System.Collections.Generic;
using UnityEngine;

public class RobotMove : MonoBehaviour {

	public Cell CurrentCell;
	private List<int> visitedCells = new List<int>();
	int position = 0;

	void Start()
	{
		position = Random.Range(0, Maze.MazeInstance.totalCells);
		InvokeRepeating("Walk", 1f, 1f);
	}

	void Update()
	{
		CurrentCell = Maze.MazeInstance.cells[position];
		transform.position = CurrentCell.Position;
		//print(string.Format("North: {0}  South: {1} East: {2} West: {3}", CurrentCell.North.activeInHierarchy, CurrentCell.South.activeInHierarchy, CurrentCell.East.activeInHierarchy, CurrentCell.West.activeInHierarchy));
	}

	void Walk()
	{
		//GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//g.transform.position = transform.position;
		if (!CurrentCell.West.activeInHierarchy && !visitedCells.Contains(position))
		{
			visitedCells.Add(position);
			CurrentCell = Maze.MazeInstance.cells[++position];
		}
		if (!CurrentCell.North.activeInHierarchy && !visitedCells.Contains(position))
		{
			visitedCells.Add(position);
			CurrentCell = Maze.MazeInstance.cells[position += Maze.MazeInstance.xSize];
		}
		if (!CurrentCell.East.activeInHierarchy && !visitedCells.Contains(position))
		{
			visitedCells.Add(position);
			CurrentCell = Maze.MazeInstance.cells[--position];
		}
		if (!CurrentCell.South.activeInHierarchy && !visitedCells.Contains(position))
		{
			visitedCells.Add(position);
			CurrentCell = Maze.MazeInstance.cells[position -= Maze.MazeInstance.xSize];
		}
	}
	
}
