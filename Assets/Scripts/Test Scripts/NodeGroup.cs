﻿using UnityEngine;
using System;

public class NodeGroup
{
    public Node[,] nodeGroup = new Node[3,3];

    Node startingNode;
    Node[,] grid;
    public Vector3 nodeGroupPosition;

    public NodeGroup(Node leftMost, Node[,] _grid)
    {
        startingNode = leftMost;
        grid = _grid;

        nodeGroup = GetGroupMembers();
    }

    public Node[,] GetGroupMembers()
    {
        Node[,] _nodeGroup = new Node[3, 3];

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                try
                {
                    _nodeGroup[i, j] = grid[startingNode.gridX + i, startingNode.gridY + j];
                }
                catch (IndexOutOfRangeException) { }
            }
        }
        return _nodeGroup;
    }

    //public override string ToString()
    //{
    //    nodeGroup = GetGroupMembers();
    //    StringBuilder name = new StringBuilder();
    //    for (int i = 0; i < 3; i++)
    //    {
    //        for (int j = 0; j < 3; j++)
    //        {
    //            name.Append(nodeGroup[i, j]);
    //        }
    //    }
    //    return name.ToString();
    //}
}
