﻿using UnityEngine;
using System;

[Serializable]
public class Node
{
    protected Grid grid;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;
    public bool walkable;
    public bool colored;

    /// <summary>
    /// Can be used for graph traversals
    /// </summary>
    public Node parent;

    public Node(Vector3 _worldPos, int _gridX, int _gridY, bool _walkable = true)
    {
        walkable = _walkable;
        worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
    }

    public bool Done { get { return walkable && colored; } }

    public override string ToString()
    {
        return gridX + ", " + gridY;
    }
}