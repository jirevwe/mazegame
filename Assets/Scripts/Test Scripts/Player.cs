﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [HideInInspector]
    public Player playerInstance;

    bool isPressed = false;
    Vector3 initialClickPos;
    Vector3 finalClickPos;
    public LayerMask whatIsPlayer;
    bool selected;
    RaycastHit hitInfo;
    GameObject objectSelected = null;
    Vector3 position = Vector3.zero;
    bool move = false;

    EasyTouch.SwipeDirection swipe;

    float startMove, endMove = 0f;

    public bool Move
    {
        get { return move; }
    }

    void Awake()
    {
        playerInstance = this;
        position = transform.position;
        Grid.instance.NodeFromWorldPoint(position).walkable = false;
        move = true;
    }

    void Update()
    {
        GetInput();
        PlayerMove();
        if (Input.GetMouseButtonUp(0))
        {
            move = !move;
        }
    }

    void OnDrawGizmos()
    {
       Gizmos.DrawCube(position, Vector3.one * Grid.instance.nodeRadius);
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "pickup")
        {
            Destroy(collider.gameObject);
        }
    }

    void GetInput()
    {
        if (Input.GetKeyUp(KeyCode.RightArrow) || swipe == EasyTouch.SwipeDirection.Right)
        {
            startMove = Time.realtimeSinceStartup;

            Vector3 positionToMoveTo = Vector3.zero;

            Node node = Grid.instance.NodeFromWorldPoint(transform.position);

            int currentX = node.gridX;
            for (; currentX < Grid.instance.gridWorldSize.x; currentX++)
            {
                positionToMoveTo = Grid.instance.grid[currentX, node.gridY].worldPosition;
            }
            move = true;

            position = positionToMoveTo;
            if (transform.position != position)
                return;
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow) || swipe == EasyTouch.SwipeDirection.Left)
        {
            startMove = Time.realtimeSinceStartup;

            Vector3 positionToMoveTo = Vector3.zero;

            Node node = Grid.instance.NodeFromWorldPoint(transform.position);

            int currentX = node.gridX;
            for (; currentX >= 0; currentX--)
            {
                positionToMoveTo = Grid.instance.grid[currentX, node.gridY].worldPosition;
            }
            move = true;

            position = positionToMoveTo;
            if (transform.position != position)
                return;
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow) || swipe == EasyTouch.SwipeDirection.Up)
        {
            startMove = Time.realtimeSinceStartup;

            Vector3 positionToMoveTo = Vector3.zero;

            Node node = Grid.instance.NodeFromWorldPoint(transform.position);

            int currentY = node.gridY;
            for (; currentY < Grid.instance.gridWorldSize.y; currentY++)
            {
                positionToMoveTo = Grid.instance.grid[node.gridX, currentY].worldPosition;
            }
            move = true;

            position = positionToMoveTo;
            if (transform.position == position)
                return;
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow) || swipe == EasyTouch.SwipeDirection.Down)
        {
            startMove = Time.realtimeSinceStartup;

            Vector3 positionToMoveTo = Vector3.zero;

            Node node = Grid.instance.NodeFromWorldPoint(transform.position);

            int currentY = node.gridY;
            for (; currentY >= 0; currentY--)
            {
                positionToMoveTo = Grid.instance.grid[node.gridX, currentY].worldPosition;
            }
            move = true;

            position = positionToMoveTo;
            if (transform.position == position)
                return;
        }
    }

    void PlayerMove()
    {
        if (transform.position != position && position != Vector3.zero && move == true)
        {
            position.y = 0.5f;
            transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * Grid.instance.moveRate);
        }
        if(transform.position == position && move == true)
        {
            move = false;
            endMove = Time.realtimeSinceStartup;
            print(endMove - startMove);
        }
    }

    #region SwipeInput

    void OnEnable()
    {
        EasyTouch.On_SwipeStart += On_SwipeStart;
        EasyTouch.On_Swipe += On_Swipe;
        EasyTouch.On_SwipeEnd += On_SwipeEnd;
    }

    void OnDisable()
    {
        UnsubscribeEvent();

    }

    void OnDestroy()
    {
        UnsubscribeEvent();
    }

    void UnsubscribeEvent()
    {
        EasyTouch.On_SwipeStart -= On_SwipeStart;
        EasyTouch.On_Swipe -= On_Swipe;
        EasyTouch.On_SwipeEnd -= On_SwipeEnd;
    }


    // At the swipe beginning 
    private void On_SwipeStart(Gesture gesture)
    {

    }

    // During the swipe
    private void On_Swipe(Gesture gesture)
    {

    }

    // At the swipe end 
    private void On_SwipeEnd(Gesture gesture)
    {
        swipe = gesture.swipe;
    }

    #endregion

    //public void PlayerOnObjective(Vector3 currentPos)
    //{
    //    Node node = Grid.instance.objectiveNodes.Find(n => n.worldPosition == currentPos);
    //    if (node != null)
    //    {
    //        int index = Grid.instance.objectiveNodes.IndexOf(node);
    //        Grid.instance.objectiveNodes.RemoveAt(index);

    //        Grid.instance.objectiveNodeGameobjects[index].SetActive(false);
    //        Grid.instance.objectiveNodeGameobjects.RemoveAt(index);
    //    }
    //}
}
