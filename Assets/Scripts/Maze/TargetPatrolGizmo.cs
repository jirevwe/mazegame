﻿using UnityEngine;
using System.Collections;

public class TargetPatrolGizmo : MonoBehaviour {
    [Range(0, 1)]
    public float radius;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, radius);
    }
}
