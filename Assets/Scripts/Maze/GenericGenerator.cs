﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System;

public class GenericGenerator : MonoBehaviour
{

    private int[][] level;
    private List<GameObject> enemies = new List<GameObject>();

    public GameObject wallPrefab;
    public GameObject tilesPrefab;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject enemyTargetPrefab;

    public TextAsset levelTextFile;

    public List<Cell> cells = new List<Cell>();

    int currX = 0;
    int currY = 1;
    int currZ = 0;

    public void LoadArray()
    {
        int[,] temp = Load(levelTextFile);

        int u1 = temp.GetUpperBound(0) + 1;
        int u2 = temp.GetUpperBound(1) + 1;

        level = new int[u1][];

        for (int i = 0;i < u1;i++)
        {
            level[i] = new int[u1];
            for(int j = 0;j < u2;j++)
            {
                level[i][j] = temp[i,j];

            }
        }
    }

    private int[,] Load(TextAsset fileName)
    {
        int[,] levelFile = null;

        try
        {
            string allLines = fileName.text;
            string[] lines = allLines.Split(new char[] { '#' });

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Trim(new char[] {'\n', '\r', '\t' });
                if (line != null)
                {
                    if (levelFile == null)
                        levelFile = new int[line.Length, line.Length];
                    else
                    {
                        for (int j = 0; j < line.Length; j++)
                        { 
                            levelFile[i, j] = Convert.ToInt32(line[j].ToString());
                        }
                    }
                }
            }
            return levelFile;
        }
        // If anything broke in the try block, we throw an exception with information
        // on what didn't work
        catch (IOException e)
        {
            Debug.LogError(string.Format("{0}\n", e.Message));
            return levelFile;
        }
    }

    // Use this for initialization
    void Start()
    {
        LoadArray();

        Vector3 pos = new Vector3();
        Cell cell;
        GameObject g;

        foreach (var x in level)
        {
            foreach (var y in x)
            {           
                pos.x = currX;
                pos.z = currZ;
                pos.y = currY;
                switch (y)
                {

                    case 0:
                        //wall
                        Instantiate(wallPrefab, pos, Quaternion.identity);
                        break;
                    case 1:
                        //cells
                        cell = new Cell();
                        cell.Position = pos;

                        g = new GameObject();
                        g.transform.position = cell.Position;
                        g.name = "Tile" + cells.ToList().IndexOf(cell);
                        g.tag = "tile";
                        g.layer = 12;
                        g.transform.parent = tilesPrefab.transform;
                        cell.CellGameObject = g;

                        cells.Add(cell);
                        break;
                    case 2:
                        //enemy and cell
                        cell = new Cell();
                        cell.Position = pos;

                        g = new GameObject();
                        g.transform.position = cell.Position;
                        g.name = "Tile" + cells.ToList().IndexOf(cell);
                        g.tag = "tile";
                        g.layer = 12;
                        g.transform.parent = tilesPrefab.transform;
                        cell.CellGameObject = g;

                        cells.Add(cell);

                        GameObject _drone = Instantiate(enemyPrefab, cell.Position, Quaternion.identity) as GameObject;
                        _drone.transform.localScale = new Vector3(.5f, .5f, .5f);
			            enemies.Add(_drone);

			            GameObject _droneTarget = Instantiate(enemyTargetPrefab, cells[UnityEngine.Random.Range(0, cells.Count)].Position, Quaternion.identity) as GameObject;
			            _drone.GetComponent<NPCStateMachineManager>().currentTarget = _droneTarget;
                        break;
                    case 3:
                        //player and cell
                        cell = new Cell();
                        cell.Position = pos;

                        g = new GameObject();
                        g.transform.position = cell.Position;
                        g.name = "Tile" + cells.ToList().IndexOf(cell);
                        g.tag = "tile";
                        g.layer = 12;
                        g.transform.parent = tilesPrefab.transform;
                        cell.CellGameObject = g;

                        cells.Add(cell);

                        GameObject player = Instantiate(playerPrefab, cell.Position, Quaternion.identity) as GameObject;
                        player.transform.localScale = new Vector3(.5f, .5f, .5f);

                        break;
                }
                currX++;
            }
            currX = 0;
            currZ++;
        }

        AstarPath.active.Scan();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
