﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Maze : MonoBehaviour {

	public static Maze MazeInstance;
	[SerializeField]
	string seed;

    [SerializeField]
    bool randomSeed;

	public GameObject Wall;
	public GameObject Ground;
	public GameObject item;
	public GameObject laser;
	public GameObject tiles;
	public GameObject propSwitch;
	public GameObject exitPrefab;
	public GameObject minicamPrefab;
	public GameObject sheildPrefab;
	public GameObject[] enemyPrefabs;
	public GameObject enemyTarget;
	
	private GameObject minicam;
	private GameObject player;

	private bool startedBuilding = false;
    [SerializeField]
    private bool doBuildMaze = false;

    public float wallLength = 5.0f;
	public float wallHeight = 5.0f;
	
	private Vector3 initialPosition;

	[HideInInspector] public int totalCells;
	[HideInInspector] public Cell[] cells;
	[HideInInspector] public GameObject exitPoint;
	[HideInInspector] public GameObject WallHolder;
	public int difficulty;

    [HideInInspector]
	public System.Random randInstance;
	public  int   xSize;
	public  int   ySize;
	private int   CurrentCell = 0;
	private int   visitedCells = 0;
	private int   CurrentNeighbour = 0;
	private int   backingUp = 0;
	private int   wallToBreak = 0;

	private List<GameObject> enemies = new List<GameObject> ();
	private List<int> lastCells = new List<int>();
	private List<GameObject> inactiveWalls = new List<GameObject>();

	private Vector3 playerStartPostion;
    List<GameObject> allTiles = new List<GameObject>();
    public GameObject playerTileLocation;

    void Awake()
	{
		MazeInstance = this;
        if (randomSeed)
            randInstance = new System.Random((int)Time.realtimeSinceStartup);
        else
            randInstance = new System.Random(seed.GetHashCode());

        player = GameObject.FindGameObjectWithTag("Player");
		tiles = GameObject.FindGameObjectWithTag("TileHolder");
	}

	void Start () {
		//difficulty = PlayerPrefs.GetInt("DIFF");

		//xSize = Random.Range (5 * difficulty, (5 * difficulty) + 1);
		//ySize = Random.Range (5 * difficulty, (5 * difficulty) + 1);

		totalCells = xSize * ySize;

        if(doBuildMaze)
		    CreateWalls();

		playerStartPostion = new Vector3 (cells[0].Position.x, 1, cells[0].Position.z);

		player.SetActive (true);
		player.transform.position = playerStartPostion;
		player.GetComponent<PlayerBehaviour>().canMove = false;
		AstarPath.active.Scan();
	}

	void CreateWalls()
	{
		//create Ground
		Instantiate(Ground, Vector3.zero, Quaternion.identity);

		WallHolder = new GameObject();
		WallHolder.name = "Maze";

		initialPosition = new Vector3((-xSize / 2f) + wallLength / 2, 10f, (-ySize / 2f) + wallLength / 2);

		//for all x axis
		for (int i = 0; i < ySize; i++)
		{
			for (int j = 0; j <= xSize; j++)
			{
				var myPos = new Vector3(initialPosition.x + (j * wallLength) - wallLength / 2, 1f, initialPosition.z + (i * wallLength) - wallLength / 2);
				var tempWall = Instantiate(Wall, myPos, Quaternion.identity) as GameObject;
                tempWall.transform.localScale = new Vector3(tempWall.transform.localScale.x, wallHeight, wallLength);
                tempWall.transform.parent = WallHolder.transform;
			}
		}

		//for all y axis
		for (int i = 0; i <= ySize; i++)
		{
			for (int j = 0; j < xSize; j++)
			{
				var myPos = new Vector3(initialPosition.x + (j * wallLength), 1f, initialPosition.z + (i * wallLength) - wallLength);
				var tempWall = Instantiate(Wall, myPos, Quaternion.Euler(0f, 90f, 0f)) as GameObject;
                tempWall.transform.localScale = new Vector3(tempWall.transform.localScale.x, wallHeight, wallLength);
                tempWall.transform.parent = WallHolder.transform;
			}
		}

		CreateCells();
	}

	void CreateCells()
	{
		lastCells.Clear();
		int childern = WallHolder.transform.childCount;
		GameObject[] allWalls = new GameObject[childern];
		cells= new Cell[xSize * ySize];
		int eastWestProcess = 0;
		int childProcess = 0;
		int termCount = 0;

		//gets all the childern
		for (int i = 0; i < childern; i++)
		{
			allWalls[i] = WallHolder.transform.GetChild(i).gameObject;
		}

		for (int cellProcess = 0; cellProcess  < cells.Length; cellProcess++)
		{
			if (termCount == xSize)
			{
				eastWestProcess++;
				termCount = 0;
			}

			cells[cellProcess] = new Cell();

			cells[cellProcess].East = allWalls[eastWestProcess];
			cells[cellProcess].South = allWalls[childProcess + (xSize + 1) * ySize];

			eastWestProcess++;

			termCount++;
			childProcess++;

			cells[cellProcess].West = allWalls[eastWestProcess];
			cells[cellProcess].North = allWalls[(childProcess + (xSize + 1) * ySize) + xSize - 1];

			cells[cellProcess].cellID = cellProcess;
		}

		CreateTiles ();
		CreateMaze  ();
		AddEnemies  ();
    }

	void CreateTiles()
	{
		foreach (Cell cell in cells)
		{
			GameObject g = new GameObject();
			g.transform.position = cell.Position;
            g.AddComponent<Tile>();
            g.AddComponent<BoxCollider>();
            g.GetComponent<BoxCollider>().size = new Vector3(wallLength, 1f, wallLength);
            g.GetComponent<BoxCollider>().isTrigger = true;
            g.name = "Tile" + cells.ToList().IndexOf(cell);
			g.tag = "tile";
			g.layer = LayerMask.NameToLayer("tile");
			g.transform.parent = tiles.transform;
			cell.CellGameObject = g;
		}

		int a = xSize - xSize;
		int b = xSize - 1;
		int c = (xSize * ySize) - 1;

		exitPoint = Instantiate(exitPrefab, cells[totalCells - 1].Position, Quaternion.identity) as GameObject;

		var A = cells [a].Position;
		var B = cells [b].Position;
		var C = cells [c].Position;

		var m = Vector3.Distance (A, B);
		var n = Vector3.Distance (B, C);

		minicamPrefab.transform.position = new Vector3(A.x + m / 2, 10, B.z + n / 2);

		switch (difficulty)
		{
			case 1:
				minicamPrefab.GetComponent<Camera>().orthographicSize = 30;
				break;
			case 2:
				minicamPrefab.GetComponent<Camera>().orthographicSize = 55;
				break;
			case 3:
				minicamPrefab.GetComponent<Camera>().orthographicSize = 80;
				break;
		}
	}

	void CreateMaze()
	{
		while(visitedCells < totalCells)
		{
			if (startedBuilding){
				GiveMeNeigbhour();
				if(cells[CurrentNeighbour].visited == false && cells[CurrentCell].visited == true)
				{
					BreakWall();
					cells[CurrentNeighbour].visited = true;
					visitedCells++;
					lastCells.Add(CurrentCell);
					CurrentCell = CurrentNeighbour;
					if(lastCells.Count > 0)
					{
						backingUp = lastCells.Count - 1;
					}
				}
			}
			else { 
				CurrentCell = randInstance.Next(0, totalCells);
				cells[CurrentCell].visited = true;
				visitedCells++;
				startedBuilding = true;
			}
		}
	}

	void GiveMeNeigbhour()
	{
		int length = 0;
		int[] neigbhours = new int[4];
		int[] connectingWall = new int[4];
		int check = 0;

		check = ((CurrentCell + 1) / xSize);
		check -= 1;
		check *= xSize;
		check += xSize;
		
		//West
		if (CurrentCell + 1 < totalCells && (CurrentCell + 1) != check)
		{
			if (cells[CurrentCell + 1].visited == false)
			{
				neigbhours[length] = CurrentCell + 1;
				connectingWall[length] = 3;
				length++;
			}
		}

		//East
		if (CurrentCell + 1 >= 0 && CurrentCell != check)
		{            if (cells[CurrentCell - 1].visited == false)
			{
				neigbhours[length] = CurrentCell - 1;
				connectingWall[length] = 2;
				length++;
			}
		}

		//North
		if (CurrentCell + xSize < totalCells)
		{
			if (cells[CurrentCell + xSize].visited == false)
			{
				neigbhours[length] = CurrentCell + xSize;
				connectingWall[length] = 1;
				length++;
			}
		}

		//South
		if (CurrentCell - xSize >= 0)
		{
			if (cells[CurrentCell - xSize].visited == false)
			{
				neigbhours[length] = CurrentCell - xSize;
				connectingWall[length] = 4;
				length++;
			}
		}

		if (length != 0)
		{
			int theChosenOne = randInstance.Next(0, length);
			CurrentNeighbour = neigbhours[theChosenOne];
			wallToBreak = connectingWall[theChosenOne];
		}
		else {
			if (backingUp > 0)
			{
				CurrentCell = lastCells[backingUp];
				backingUp--;
			}
		}
	}

	void BreakWall()
	{
		switch (wallToBreak)
		{ 
			case 1:
				cells[CurrentCell].North.SetActive(false);
				inactiveWalls.Add(cells[CurrentCell].North);
				break;
			case 2:
				cells[CurrentCell].East.SetActive(false);
				inactiveWalls.Add(cells[CurrentCell].East);
				break;
			case 3:
				cells[CurrentCell].West.SetActive(false);
				inactiveWalls.Add(cells[CurrentCell].West);
				break;
			case 4:
				cells[CurrentCell].South.SetActive(false);
				inactiveWalls.Add(cells[CurrentCell].South);
				break;
		}
	}

	void AddEnemies(){
		int count = ((xSize > ySize ? xSize : ySize) / 2) + 1;
		for (int i = 0;i < count;i++) {
			var temp = cells[randInstance.Next(0, cells.Length)].Position;

			//create the drone target
			GameObject _enemyTarget = Instantiate(enemyTarget, cells[randInstance.Next(0, cells.Length)].Position, Quaternion.identity) as GameObject;

			//create the drone
			GameObject _enemy = Instantiate(enemyPrefabs[randInstance.Next(0, enemyPrefabs.Length)], temp == playerStartPostion ? cells[randInstance.Next(0, cells.Length)].Position : temp, Quaternion.identity) as GameObject;
			enemies.Add(_enemy);
			_enemy.GetComponent<NPCStateMachineManager>().currentTarget = _enemyTarget;
		}
	}

	void AddShield()
	{
		for (int i = 0;i < Mathf.Pow(difficulty, 2) * 2; i++)
		{
			var g = Instantiate(sheildPrefab, cells[randInstance.Next(0, cells.Length)].Position, Quaternion.Euler(-90, 0, 0)) as GameObject;
			g.transform.localScale = new Vector3(.5f, .5f, .5f);
		}
	}

	void AddObjectives (){

		int a = xSize - xSize;
		int b = xSize - 1;
		int c = (xSize * ySize) - 1;
		int d = (xSize * ySize) - xSize;

		Debug.Log (string.Format("{0}, {1}, {2}, {3}", a, b, c, d));

		Instantiate (propSwitch, cells[a].Position, Quaternion.identity);
		Instantiate (propSwitch, cells[b].Position, Quaternion.identity);
		Instantiate (propSwitch, cells[c].Position, Quaternion.identity);
		Instantiate (propSwitch, cells[d].Position, Quaternion.identity);
	}

    public GameObject GetTileFromWorldPoint(Vector3 worldPoint)
    {
        if (allTiles.Count == 0)
        {
            //register tile gameobjects
            foreach (Transform tile in tiles.transform)
            {
                allTiles.Add(tile.gameObject);
            }
        }
        return allTiles.Find(n => n.transform.position == worldPoint);
    }

    void _OnDrawGizmos()
    {
        for (int i = 0; i < cells.Length; i++)
        {
            foreach (var cell in cells[i].SubCells)
            {
                Gizmos.DrawWireCube(cell, Vector3.one * wallLength / 3.5f);
            }
        }
    }
}

[System.Serializable]
public class Cell
{
    public Vector3 pos = Vector3.zero;

    public int cellID;
    int[,] subCells = new int[3,3];

    public bool visited;
    public GameObject North;//1
    public GameObject East; //2
    public GameObject West; //3
    public GameObject South;//4

    public Vector3 NorthTransform { get { return North.transform.position; } }
    public Vector3 EastTransform { get { return East.transform.position; } }
    public Vector3 WestTransform { get { return West.transform.position; } }
    public Vector3 SouthTransform { get { return South.transform.position; } }

    public GameObject CellGameObject { get; set; }

    public Vector3 Position
    {
        get
        {
            float east = East.transform.position.x;
            float west = West.transform.position.x;
            float north = North.transform.position.z;
            float south = South.transform.position.z;

            return new Vector3((west + east) / 2, 0f, (north + south) / 2);
        }
        set { pos = value; }
    }

    public Vector3[,] GetSubCells(float width)
    {
        Vector3[,] map = new Vector3[3, 3];

        //upper row
        map[0, 0] = new Vector3(Position.x - width / 3, 0f, Position.z + width / 3);
        map[0, 1] = new Vector3(Position.x, 0f, Position.z + width / 3);
        map[0, 2] = new Vector3(Position.x + width / 3, 0f, Position.z + width / 3);

        //mid row
        map[1, 0] = new Vector3(Position.x - width / 3, 0f, Position.z);
        map[1, 1] = Position;
        map[1, 2] = new Vector3(Position.x + width / 3, 0f, Position.z);

        //third row
        map[2, 0] = new Vector3(Position.x - width / 3, 0f, Position.z - width / 3);
        map[2, 1] = new Vector3(Position.x, 0f, Position.z - width / 3);
        map[2, 2] = new Vector3(Position.x + width / 3, 0f, Position.z - width / 3);

        return map;
    }

    public Vector3[,] SubCells
    {
        get
        {
            return GetSubCells(5f);
        }
    }
}