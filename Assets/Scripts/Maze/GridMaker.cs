﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GridMaker : MonoBehaviour {

    List<GameObject> horizontalWalls = new List<GameObject>();
    List<GameObject> verticalWalls = new List<GameObject>();
    GameObject[,] nodes;

    [SerializeField]
    private GameObject horWall;
    [SerializeField]
    private GameObject verWall;
    [SerializeField]
    private GameObject node;

    [SerializeField]
    private Vector3 StartPoint;

    public float noOFSquares;

    void DrawH()
    {
        float offsetH = 3.5f;
        float offsetV = 3.5f;

        for (int i = 0; i < noOFSquares; i++)
        {
            offsetH = 3.5f;
            for (int j = 0; j < noOFSquares; j++)
            {
                GameObject h = Instantiate(horWall, new Vector3(StartPoint.x + offsetV, StartPoint.y, StartPoint.z - offsetH), Quaternion.identity) as GameObject;
                horizontalWalls.Add(h);
                offsetH += 3.5f;
            }
            offsetV += 3.5f;   
        }
        offsetH = 3.5f;
        for (int i = 0; i < noOFSquares; i++)
        {
            GameObject hh = Instantiate(horWall, new Vector3(StartPoint.x, StartPoint.y, StartPoint.z - offsetH), Quaternion.identity) as GameObject;
            horizontalWalls.Add(hh);
            offsetH += 3.5f;
        }
    }

    void DrawNodes()
    {
        float offsetH = 3.5f;
        float offsetV = 3.5f;
        StartPoint += new Vector3(0.5f, 0, -2.34f);

        for (int i = 0; i < noOFSquares; i++)
        {
            offsetH = 3.5f;
            for (int j = 0; j < noOFSquares; j++)
            {
                GameObject n = Instantiate(node, new Vector3(StartPoint.x + offsetV, StartPoint.y, StartPoint.z - offsetH), Quaternion.identity) as GameObject;
                n.GetComponent<EdgeMaker>().Priority = 100f;
                nodes[i, j] = n;
                offsetH += 3.5f;
            }
            offsetV += 3.5f;
        }
    }

    void DrawV()
    {
        float offsetH = 3.5f;
        float offsetV = 3.5f;
        StartPoint += new Vector3(-1.5f, 0, 2.34f);

        for (int i = 0; i < noOFSquares; i++)
        {
            offsetV = 3.5f;
            for (int j = 0; j < noOFSquares; j++)
            {
                GameObject v = Instantiate(verWall, new Vector3(StartPoint.x + offsetV, StartPoint.y, StartPoint.z - offsetH), Quaternion.Euler(0, 90, 0)) as GameObject;
                verticalWalls.Add(v);
                offsetV += 3.5f;
            }
            offsetH += 3.5f;
        }
        offsetV = 3.5f;
        for (int i = 0; i < noOFSquares; i++)
        {
            GameObject vv = Instantiate(verWall, new Vector3(StartPoint.x + offsetV, StartPoint.y, StartPoint.z - offsetH), Quaternion.Euler(0, 90, 0)) as GameObject;
            verticalWalls.Add(vv);
            offsetV += 3.5f;
        }
    }

    void Prim()
    {
        List<GameObject> queue = new List<GameObject>();
        List<GameObject> MST = new List<GameObject>();
        
        for (int i = 0; i < noOFSquares; i++)
            for (int j = 0; j < noOFSquares; j++)
                queue.Add(nodes[i, j]);

        int c = queue.Count;
        MST.Add(queue[0]);


        while (queue.Count > 0){
            
            
        } 
    }

    void Awake()
    {
        nodes = new GameObject[(int)noOFSquares, (int)noOFSquares];
        DrawH(); DrawV(); DrawNodes();
    }
}
