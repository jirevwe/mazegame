﻿using UnityEngine;
using System.Collections;

public class CollisionTileBehaviour : MonoBehaviour {

    private bool sent = false;
    private string myName;
    private string myParentName;

    void Awake()
    {
        myName = name[3].ToString();
        myParentName = transform.parent.gameObject.name[3].ToString();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Robot")
        {
            if (sent == false)
            {
                string[] details = { myName, myParentName };
                col.gameObject.SendMessage("Pattern", details, SendMessageOptions.RequireReceiver);
                sent = true;
            }
        }
    }


    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Robot")
        {
                string[] details = { myName, myParentName };
                col.gameObject.SendMessage("Leave", details, SendMessageOptions.RequireReceiver);
                sent = false;
        }
    }

}
