﻿using UnityEngine;
using System.Collections;

public class EdgeMaker : MonoBehaviour {

    GameObject[] neigbhours;
    public RaycastHit[] hit = new RaycastHit[4];
    public int[] weights = new int[4];

    private float priority = 0;
    public float Priority {
        get { return priority; }
        set { priority = value; }
    }

	// Update is called once per frame
	void Update () {
	    if(Physics.Raycast(transform.position, transform.forward, out hit[0], 4))
        {
            weights[0] = Random.Range(1, 11);
            Debug.DrawRay(transform.position, transform.forward * 4);
        }
        if (Physics.Raycast(transform.position, -transform.forward, out hit[1], 4))
        {
            weights[1] = Random.Range(1, 11);
            Debug.DrawRay(transform.position, -transform.forward * 4);
        }
        if (Physics.Raycast(transform.position, transform.right, out hit[2], 4))
        {
            weights[2] = Random.Range(1, 11);
            Debug.DrawRay(transform.position, transform.right * 4);
        }
        if (Physics.Raycast(transform.position, -transform.right, out hit[3], 4))
        {
            weights[3] = Random.Range(1, 11);
            Debug.DrawRay(transform.position, -transform.right * 4);
        }
	}
}