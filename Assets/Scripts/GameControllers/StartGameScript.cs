﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Globalization;
using UnityEngine.SceneManagement;

public class StartGameScript : MonoBehaviour {

    public Text easy;
    public Text normal;
    public Text hard;

	// Use this for initialization
	void Awake () {

        //Debug.LogError(StringManager.GAME_TIME + 1);
        //Debug.LogError(StringManager.GAME_TIME + 2);
        //Debug.LogError(StringManager.GAME_TIME + 3);

        easy.text   = String.Format(CultureInfo.InvariantCulture, "Best Time: {0:00}:{1:00}", (int)PlayerPrefs.GetFloat(StringManager.GAME_TIME + 1) / 60, (int)PlayerPrefs.GetFloat(StringManager.GAME_TIME + 1) % 60);
        normal.text = String.Format(CultureInfo.InvariantCulture, "Best Time: {0:00}:{1:00}", (int)PlayerPrefs.GetFloat(StringManager.GAME_TIME + 2) / 60, (int)PlayerPrefs.GetFloat(StringManager.GAME_TIME + 2) % 60);
        hard.text   = String.Format(CultureInfo.InvariantCulture, "Best Time: {0:00}:{1:00}", (int)PlayerPrefs.GetFloat(StringManager.GAME_TIME + 3) / 60, (int)PlayerPrefs.GetFloat(StringManager.GAME_TIME + 3) % 60);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartGameEasy()
    {
        PlayerPrefs.SetInt("DIFF", 1);
        PlayerPrefs.Save();
        SceneManager.LoadScene("1", LoadSceneMode.Single);
    }

    public void StartGameNormal()
    {
        PlayerPrefs.SetInt("DIFF", 2);
        PlayerPrefs.Save();
        SceneManager.LoadScene("1", LoadSceneMode.Single);
    }

    public void StartGameHard()
    {
        PlayerPrefs.SetInt("DIFF", 3);
        PlayerPrefs.Save();
        SceneManager.LoadScene("1", LoadSceneMode.Single);
    }
}
