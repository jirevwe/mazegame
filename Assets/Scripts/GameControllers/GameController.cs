﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController GameControllerInstance;

	[SerializeField] private Text deadText;
	[SerializeField] private Text gameTimeText;

	private string gameTimeString;

	[HideInInspector]public GameObject playerPrefab;

	public Material active;
	public Material inactive;

	[HideInInspector]public float playerSeenCounter = 0f;

	public bool gamePaused = false;

	[SerializeField]private Canvas gameCanvas;

	[SerializeField]private Canvas pauseCanvas;

	[HideInInspector]
	public enum PlayerSightedModes
	{
		Seen,
		NotSeen
	}

	public Button InputModeSwitch;

	public enum NavigationMode
	{
		Player,
		Map
	}

	[HideInInspector] public List<FieldOfView> visible = new List<FieldOfView>();

	[HideInInspector] public GameObject[] guards;

	public PlayerSightedModes playerSightedMode = PlayerSightedModes.NotSeen;
	
	public NavigationMode currentNavigatioMode = NavigationMode.Player;

	private float gameTime = 0f;
	[HideInInspector]
	public GameObject[] playerLevelSpawnPoints;
	[HideInInspector]
	public float depletionRate = 1f;

	float Count = 2f;
	float c = 0f;

	public Sprite map, player;

	public float GameTime
	{
		get { return gameTime; }
		set { gameTime = value; }
	}

	void OnEnable()
	{
		gamePaused = false;
		Time.timeScale = 1;             
	}

	void Awake()
	{
		playerPrefab = GameObject.FindGameObjectWithTag("Player");
		GameControllerInstance = this;
	}

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Counter", 1f, 1f);
		//InvokeRepeating ("init", .1f, .1f);
	}

	void CheckPlayerFound(){
        if (guards.Length > 0)
        {
            foreach (GameObject item in guards)
            {
                if (item.GetComponent<FieldOfView>().visibleTargets.Count > 0)
                {
                    playerSightedMode = PlayerSightedModes.Seen;
                    break;
                }
                else
                {
                    playerSightedMode = PlayerSightedModes.NotSeen;
                }
            }
        }
	}

	public void DisplayGameCompleteText()
	{
		deadText.gameObject.SetActive(true);
		deadText.text = "YOU ESCAPED...";
		StartCoroutine (Die(1));
	}

	public void DisplayPlayerDeadText()
	{
		deadText.text = "DEAD... TRY AGAIN";
		StartCoroutine (Die(3));
		deadText.gameObject.SetActive(true);
	}

	public void SwapNavigationModes()
	{
		currentNavigatioMode = currentNavigatioMode == NavigationMode.Map ? NavigationMode.Player : NavigationMode.Map;
		InputModeSwitch.GetComponent<Image>().sprite = currentNavigatioMode == NavigationMode.Map ? map : player;
		Camera.main.gameObject.GetComponent<CameraFollow>().enabled = currentNavigatioMode == NavigationMode.Player;
	}

	void init(){
		if (guards.Length == 0)
			guards = GameObject.FindGameObjectsWithTag ("Robot");
		else
			CancelInvoke ("init");
	}

	void Counter() {
		gameTime += depletionRate;
		gameTimeText.text = "      " + gameTimeString;
	}

	private IEnumerator Die(float time){
		yield return new WaitForSeconds (time);
		SceneManager.LoadScene("7_7_1", LoadSceneMode.Single);
	}

	public void Unpause(){
		gamePaused = false;
		Time.timeScale = 1;
	}

	void Update(){
		if(c >= Count){
			//CheckPlayerFound();
			c = 0f;
		}
		c += Time.deltaTime;

		gameCanvas.gameObject.SetActive(!gamePaused);
		pauseCanvas.gameObject.SetActive(gamePaused);

		if (Input.GetKeyUp (KeyCode.E) || Input.GetKeyUp(KeyCode.Escape)) {
			gamePaused = !gamePaused;
			Time.timeScale = gamePaused ? 0f : 1.0f;
		}

		gameTimeString = string.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00}", (int)gameTime/60, (int)gameTime%60);

		if (playerSightedMode == PlayerSightedModes.NotSeen) {
			Maze.MazeInstance.exitPoint.GetComponent<MeshRenderer>().material = active;
			depletionRate = 1f;
		} else if (playerSightedMode == PlayerSightedModes.Seen) {
			depletionRate = 2f;
			//Maze.MazeInstance.exitPoint.GetComponent<MeshRenderer>().material = inactive;
		}
	}
}