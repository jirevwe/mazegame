﻿using UnityEngine;
using System.Collections.Generic;

public class ShieldTimer : MonoBehaviour {

    float shieldTime = 5f;
    [SerializeField]
    PlayerBehaviour player;

    void Start()
    {
        shieldTime = player.shieldTime;
    }

	// Use this for initialization
	void OnEnable () {
        Invoke("Die", shieldTime);
        player.gameObject.layer = LayerMask.NameToLayer("invisible");
	}

    void Die()
    {
        player.gameObject.layer = LayerMask.NameToLayer("Player"); ;
        gameObject.SetActive(false);
    }
}
