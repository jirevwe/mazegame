﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    private float height;
    private GameObject player;
    private Vector3 tempPos;

	// Use this for initialization
	void Awake () {
        tempPos = new Vector3(0, height, 0);
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
        else
        {
            if (!GameController.GameControllerInstance.gamePaused)
            {
                tempPos.x = player.transform.position.x;
                tempPos.z = player.transform.position.z;
                tempPos.y = height;
                transform.position = tempPos;
                transform.LookAt(player.transform);
                Camera.main.fieldOfView = 60f;
            }
        }
	}
}
