using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    [SerializeField]
    PlayerBehaviour player;
    bool isDoubleTap = false;

    void OnEnable()
    {
        EasyTouch.On_SwipeStart += On_SwipeStart;
        EasyTouch.On_Swipe += On_Swipe;
        EasyTouch.On_SwipeEnd += On_SwipeEnd;

        EasyTouch.On_DoubleTap += On_DoubleTap;
        EasyTouch.On_SimpleTap += On_SimpleTap;

        EasyTouch.On_Drag += On_Drag;
        EasyTouch.On_Pinch += On_Pinch;
    }

    void UnsubscribeEvent()
    {
        EasyTouch.On_SwipeStart -= On_SwipeStart;
        EasyTouch.On_Swipe -= On_Swipe;
        EasyTouch.On_SwipeEnd -= On_SwipeEnd;

        EasyTouch.On_DoubleTap -= On_DoubleTap;
        EasyTouch.On_SimpleTap -= On_SimpleTap;

        EasyTouch.On_Drag -= On_Drag;
        EasyTouch.On_Pinch -= On_Pinch;
    }

    void OnDisable()
    {
        UnsubscribeEvent();
    }

    void OnDestroy()
    {
        UnsubscribeEvent();
    }

    // At the swipe beginning 
    private void On_SwipeStart(Gesture gesture)
    {

    }

    // During the swipe
    private void On_Swipe(Gesture gesture)
    {
        if (GameController.GameControllerInstance.gamePaused)
        {
            Camera.main.transform.Translate(Vector3.left * 10 * gesture.deltaPosition.x / Screen.width);
            Camera.main.transform.Translate(Vector3.down * 10 * gesture.deltaPosition.y / Screen.height);
        }
    }

    // At the swipe end 
    private void On_SwipeEnd(Gesture gesture)
    {
        player.SwipeDirection = gesture.swipe;
    }

    private void On_SimpleTap(Gesture gesture)
    {
        player.canMove = !player.canMove;
    }

    private void On_DoubleTap(Gesture gesture)
    {
        player.EnableShield();
    }

    void On_Pinch(Gesture gesture)
    {
        if (GameController.GameControllerInstance.gamePaused)
        {
            Camera.main.fieldOfView -= gesture.deltaPinch * 0.1111f;
            if (Camera.main.fieldOfView < 10f)
                Camera.main.fieldOfView = 10f;
            if (Camera.main.fieldOfView > 120f)
                Camera.main.fieldOfView = 120f;
        }

    }

    void On_Drag(Gesture gesture)
    {
        if (GameController.GameControllerInstance.gamePaused)
            On_Swipe(gesture);
    }
}