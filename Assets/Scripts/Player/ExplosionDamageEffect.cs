﻿using UnityEngine;

public class ExplosionDamageEffect : MonoBehaviour {

	public LayerMask whatIsPlayer;
	public float damageRadius = 5;

	public GameObject ragdollPrefab;
    bool done = false;

	// Use this for initialization
	void Start () {
		var targets = Physics.OverlapSphere (transform.position, damageRadius, whatIsPlayer);
		foreach (var target in targets) {
			if (target.tag == "Player")
			{
				target.gameObject.SetActive(false);
                done = true;

				GameObject deadPlayer = Instantiate(ragdollPrefab, target.transform.position, Quaternion.identity) as GameObject;
				deadPlayer.gameObject.GetComponentInChildren<Rigidbody>().AddRelativeForce(transform.rotation.eulerAngles + target.transform.rotation.eulerAngles, ForceMode.Impulse);

				StartCoroutine(PlayerBehaviour.instance.Die());
				GameController.GameControllerInstance.playerSightedMode = GameController.PlayerSightedModes.NotSeen;
				break;
			}
		}
	}

    void _OnDrawGizmos()
    {
        if(done)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, damageRadius);
        }
    }
}
