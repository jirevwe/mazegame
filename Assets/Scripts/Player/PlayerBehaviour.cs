﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerBehaviour : MonoBehaviour
{
	public static PlayerBehaviour instance;

	// Cache the inputs.
	float h = 0;
	float v = 0;

	private Animator anim;
	public float turnSmoothing = 5f;	// A smoothing value for turning the player.
	public float moveSpeed = 0.1f;
	protected float health = 30f;
    public float shieldTime = 5f;
    [HideInInspector]
    public float startClick, endClick = 0f;
    public float singleClickThreshold = 0.01f;

    public bool canMove = true;
    [SerializeField]
    bool isPressed = false;
    public bool isTwoFinger = false;

    Vector3 newDirection;
    [HideInInspector]
    public Vector3 initialPos = Vector3.zero;
    [HideInInspector]
    public Vector3 finalPos = Vector3.zero;
	
	public GameObject shield;

	public EasyTouch.SwipeDirection SwipeDirection = new EasyTouch.SwipeDirection();

	void Awake ()
	{
		instance = this;
		anim = GetComponent<Animator>();
	}

	void Update()
	{
        if (GameController.GameControllerInstance.gamePaused)
            return;

        h = CrossPlatformInputManager.GetAxis("Horizontal");
        if (Input.GetAxis("Horizontal") != 0)
            h = Input.GetAxis("Horizontal");

        v = CrossPlatformInputManager.GetAxis("Vertical");
        if (Input.GetAxis("Vertical") != 0)
            v = Input.GetAxis("Vertical");

        switch (SwipeDirection)
        {
            case EasyTouch.SwipeDirection.Down:
                h = 0; v = -1;
                break;

            case EasyTouch.SwipeDirection.Left:
                h = -1; v = 0;
                break;

            case EasyTouch.SwipeDirection.Right:
                h = 1; v = 0;
                break;

            case EasyTouch.SwipeDirection.Up:
                h = 0; v = 1;
                break;

            case EasyTouch.SwipeDirection.None:
                h = 0; v = 0;
                break;
        }

        ChangeMovementDirection(h, v);

		if(!canMove)
			anim.SetFloat("Speed", 0f);
		else
			anim.SetFloat("Speed", 5.4f);

        //GetCurrentPlayerCell();
    }

	void FixedUpdate ()
	{   
		//Now move the player
		if (canMove)
			GetComponent<Rigidbody>().MovePosition(transform.position + (transform.forward * moveSpeed));
	}

	void LateUpdate()
	{
        transform.rotation = Quaternion.Euler(newDirection);
	}

	public void ChangeMovementDirection (float horizontal, float vertical)
	{
        #region Movement Code

        Vector3 temp = transform.localRotation.eulerAngles;

        //right angled movements
        if (horizontal > 0 && vertical == 0)
		{//right
			temp.y = 90;
		}
		else if (horizontal == 0 && vertical > 0)
		{//up
			temp.y = 0;
		}
		else if (horizontal < 0 && vertical == 0)
		{//left
			temp.y = 270;
		}
		else if (horizontal == 0 && vertical < 0)
		{//down
			temp.y = 180;
		}

        newDirection = temp;

		#endregion
	}

	public void EnableShield()
	{
		shield.SetActive(true);
	}

	void OnTriggerEnter(Collider col)
	{
		
	} 

	void OnTriggerStay(Collider col)
	{
		if(col.tag == "exit"){
			if(GameController.GameControllerInstance.playerSightedMode == GameController.PlayerSightedModes.NotSeen){
				GameController.GameControllerInstance.DisplayGameCompleteText();
				if (PlayerPrefs.GetFloat(StringManager.GAME_TIME + Maze.MazeInstance.difficulty) > GameController.GameControllerInstance.GameTime ||
				 PlayerPrefs.GetFloat(StringManager.GAME_TIME + Maze.MazeInstance.difficulty) == 0)
				{
					PlayerPrefs.SetFloat(StringManager.GAME_TIME + Maze.MazeInstance.difficulty, GameController.GameControllerInstance.GameTime);
					PlayerPrefs.Save();
				}
			}else if(GameController.GameControllerInstance.playerSightedMode == GameController.PlayerSightedModes.Seen){
				//I dunno what to do here yet
			}
		}
	}

	void OnTriggerExit(Collider col)
	{

	}

    void GetCurrentPlayerCell()
    {
        float shortest = 100000f;
        for (int i = 0; i < Maze.MazeInstance.cells.Length; i++)
        {
            var newDist = Vector3.Distance(transform.position, Maze.MazeInstance.cells[i].Position);
            //print(newDist + ", " + shortest);
            if (newDist < shortest)
            {
                shortest = newDist;
                //print(Maze.MazeInstance.GetTileFromWorldPoint(Maze.MazeInstance.cells[i].Position).name);
            }
        }
    }

	public IEnumerator Die()
	{
        yield return new WaitForSeconds(.1f);
		if(canMove)
			anim.SetTrigger("Dead");
		canMove = false;
    }
}
