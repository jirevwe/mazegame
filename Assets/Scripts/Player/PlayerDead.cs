﻿using UnityEngine;
using System.Collections;

public class PlayerDead : MonoBehaviour {

	public void Awake()
    {
        GameController.GameControllerInstance.DisplayPlayerDeadText();
    }

}
