﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player")
        {
            col.gameObject.SendMessage("EnableShield");
            gameObject.SetActive(false);
        }
    }
}
